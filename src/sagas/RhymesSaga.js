import { takeEvery, call, put } from "@redux-saga/core/effects";
import Services from '../services/RhymesServices'
import { listRhymesRequest, listRhymesSuccess, listRhymesFail } from '../reducers/RhymesReducer'
import { RhymesActions } from '../actions/RhymesAction'

function* fetchListRhymes(action) {
    try {
        yield put(listRhymesRequest())
        let result = yield call(Services.get, action.word)
        yield put(listRhymesSuccess(result.data))
        
    } catch (error) {
        yield put(listRhymesFail(error.message))
    }
}

export default function* RhymesSaga() {
    console.log('RhymesSaga is running')
    yield takeEvery(RhymesActions.RHYMES_LIST_REQUEST, fetchListRhymes)
}