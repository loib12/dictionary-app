import { RHYMES_LIST_REQUEST } from '../constants/rhymesConstant'

export const RhymesActions = {
    RHYMES_LIST_REQUEST: RHYMES_LIST_REQUEST,
}