import { SYNONYMS_LIST_REQUEST, SYNONYMS_LIST_SUCCESS, SYNONYMS_LIST_FAIL} from '../constants/synonymsConstant'
import Services from '../services/SynonymsServices'


export const listSynonymsAction = (word) => async (dispatch) => {
    try {
        dispatch({ type: SYNONYMS_LIST_REQUEST })

        const { data } = await Services.get(word)

        dispatch({ 
            type: SYNONYMS_LIST_SUCCESS,
            payload: data
        })
        
    } catch (error) {
        dispatch({
            type: SYNONYMS_LIST_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}