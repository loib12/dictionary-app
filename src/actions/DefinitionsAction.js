import { DEFINITIONS_LIST_REQUEST, DEFINITIONS_LIST_SUCCESS, DEFINITIONS_LIST_FAIL } from '../constants/definitionsConstant'
import Services from '../services/DefinitionsServices'


export const listDefinitionsAction = (word) => async (dispatch) => {
    try {
        dispatch({ type: DEFINITIONS_LIST_REQUEST })

        const { data } = await Services.get(word)

        dispatch({ 
            type: DEFINITIONS_LIST_SUCCESS,
            payload: data
        })
        
    } catch (error) {
        dispatch({
            type: DEFINITIONS_LIST_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}
