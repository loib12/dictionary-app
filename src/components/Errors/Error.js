import React from 'react';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import { makeStyles } from "@material-ui/core/styles";

function Error(props) {
    const {errorDefinitions, errorSynonyms} = props

    const checkErrorEmpty = (error) => {
        if(Object.keys(error).length === 0) {
            return true
        }
        return false
    }

    let customError = ''
    if(checkErrorEmpty(errorDefinitions) && checkErrorEmpty(errorSynonyms) ) {
        customError = <Alert severity="info">please type a word</Alert>
    } else if(props.errorDefinitions || props.errorSynonyms || props.errorRhymes) {
        customError = <Alert severity="error">not found your word</Alert>
    }

    const useStyles = makeStyles((theme) => ({
        stack: {
            width: '100%'
        },
    }));
    
    const classes = useStyles(props);
    return (
        <Stack className={classes.stack}>
            {customError}
        </Stack>
    );
}

export default Error;