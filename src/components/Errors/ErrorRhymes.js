import React from 'react'
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import { makeStyles } from "@material-ui/core/styles";

function ErrorRhymes(props) {
    const { severity, children } = props

    const useStyles = makeStyles((theme) => ({
        stack: {
            width: '100%'
        },
    }));
    
    const classes = useStyles(props);
    return (
        <Stack className={classes.stack}>
            <Alert severity={severity}>{children}</Alert>
        </Stack>
    )
}

export default ErrorRhymes
