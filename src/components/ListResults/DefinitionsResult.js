import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { List } from './Li.styled'

function DefinitionsResult(props) {
  const { category, data } = props;

  const [word, setWord] = useState("");
  const [listResults, setListResults] = useState([]);

  useEffect(() => {
    if (data) {
      setWord(data.word);
      setListResults(data.definitions);
    }
  }, [data]);

  const stylePr8 = {
    paddingRight: "8px",
  }

  return (
    <>
      <List>
        <b>
          {category}: {word}
        </b>
        {listResults.slice(0, 1).map((result, index) => (
          <ul key={index}>
            <li>
              definition:
              <ul style={stylePr8}>
                <li>{result.definition}</li>
              </ul>
            </li>
            <li>
              part of speech:
              <ul>
                <li>{result.partOfSpeech}</li>
              </ul>
            </li>
          </ul>
        ))}
      </List>
    </>
  );
}

DefinitionsResult.propTypes = {
  category: PropTypes.string.isRequired,
  data: PropTypes.shape({
    word: PropTypes.string.isRequired,
    definitions: PropTypes.array.isRequired,
  }),
};

export default DefinitionsResult;
