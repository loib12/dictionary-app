import styled from 'styled-components'

export const List = styled.li`
    list-style-type: none;
    margin-top: 20px;
`