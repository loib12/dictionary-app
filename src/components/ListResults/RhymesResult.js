import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { List } from './Li.styled'


function RhymesResult(props) {
  const { category, data } = props;
  const [word, setWord] = useState("");
  const [listResults, setListResults] = useState([]);

  useEffect(() => {
    if (data.hasOwnProperty("rhymes")) {
      const { rhymes } = data;
      setWord(data.word);
      data.rhymes.hasOwnProperty("all") &&
        setListResults(rhymes.all.slice(0, 5));
    }
  }, [data]);

  const useStyles = makeStyles((theme) => ({
    noneDot: {
      listStyleType: "none",
    },
  }));

  const classes = useStyles(props);

  return (
    <>
      <List>
        <b>
          {category}: {word}
        </b>
        <ul>
          {
            listResults.map((result, index) => <li key={index}>{result}</li>)
          }
        </ul>
      </List>
    </>
  );
}

RhymesResult.propTypes = {
  category: PropTypes.string.isRequired,
  data: PropTypes.shape({
    word: PropTypes.string.isRequired,
    rhymes: PropTypes.object.isRequired
  }),
};

export default RhymesResult;
