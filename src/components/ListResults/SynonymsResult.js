import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { List } from './Li.styled'


function SynonymsResult(props) {
  const { category, data } = props
  const [word, setWord] = useState("");
  const [listResults, setListResults] = useState([]);

  useEffect(() => {
    if (data) {
      const { synonyms } = data
      setWord(data.word);
      data.synonyms && 
        setListResults(synonyms.slice(0, 5))
    }
  }, [data]);

  return (
    <>
      <List>
        <b>
          {category}: {word}
        </b>
        <ul>
          {listResults.map((result, index) => (
            <li key={index}>{result}</li>
          ))}
        </ul>
      </List>
    </>
  );
}

SynonymsResult.propTypes = {
  category: PropTypes.string.isRequired,
  data: PropTypes.shape({
    word: PropTypes.string.isRequired,
    synonyms: PropTypes.array.isRequired,
  }),
};

export default SynonymsResult;
