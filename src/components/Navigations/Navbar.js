import React from 'react'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Logo from '../../assets/images/logo.png'
import { makeStyles } from '@mui/styles';
import { Link } from "react-router-dom";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery'

import DrawerComponent from './Drawer';

const useStyles = makeStyles({
    button: {
        color: '#000 !important',
        display: 'hidden !important',
        '&:hover': {
            color: '#ed5f5e !important',
            cursor: 'pointer !important'
        }
    },
    appBar: {
        backgroundColor: 'white !important',
        padding: '0 50px 0 50px'
    },
    toolBar: {
        justifyContent: 'space-between !important',
    },
    box: {
        display: 'flex',
        justifyContent: 'flex-end',
        flexGrow: 1
    },
    link: {
        textDecoration: 'none',
        color: '#000',
        '&:hover': {
            color: '#ed5f5e !important',
            cursor: 'pointer !important'
        }
    }
});

function Navbar(props) {
    const classes = useStyles(props);
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));

    return (
        <Box>
            <AppBar position="static" className={classes.appBar} >
                <Toolbar className={classes.toolBar}>
                    <Link to="/" className={classes.link}>
                        <img src={Logo} alt="logo"/>
                    </Link>
                    {isMobile ? (
                        <DrawerComponent />
                    ) : (
                        <Box className={classes.box}>
                            <Link to="/" className={classes.link} style={{ marginRight: '24px' }}>
                                <Typography>Home</Typography>
                            </Link>
                            <Link to="/lookup" className={classes.link}>
                                <Typography>Dictionary Lookup</Typography>
                            </Link>
                        </Box>
                    )}
                </Toolbar>
            </AppBar>
        </Box>
    )
}
export default Navbar
