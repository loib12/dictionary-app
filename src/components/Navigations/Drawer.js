import React, { useState } from "react";
import { Divider, Drawer, IconButton, List, ListItem, ListItemText, makeStyles,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { Menu } from "@mui/icons-material";
import useToggle from "../../customHooks/useToggle";

const useStyles = makeStyles(() => ({
  link: {
    textDecoration: "none",
    color: "#000",
    "&:hover": {
      color: "#ed5f5e !important",
      cursor: "pointer !important",
    },
  },
  icon: {
    color: "#000",
  },
}));

function DrawerComponent() {
  const classes = useStyles();
  const [openDrawer, setOpenDrawer] = useToggle(false);
  return (
    <>
      <Drawer open={openDrawer} onClose={() => setOpenDrawer(false)}>
        <List>
          <ListItem onClick={() => setOpenDrawer(false)}>
            <ListItemText>
              <Link to="/" className={classes.link}>
                Home
              </Link>
            </ListItemText>
          </ListItem>
          <Divider />
          <ListItem onClick={() => setOpenDrawer(false)}>
            <ListItemText>
              <Link to="/lookup" className={classes.link}>
                Dictionary Lookup
              </Link>
            </ListItemText>
          </ListItem>
          <Divider />
        </List>
      </Drawer>
      <IconButton
        onClick={setOpenDrawer}
        className={classes.icon}
      >
        <Menu />
      </IconButton>
    </>
  );
}
export default DrawerComponent;
