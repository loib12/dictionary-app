import React from 'react'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { makeStyles } from "@material-ui/core/styles";


function CardItem(props) {
    const useStyles = makeStyles(theme => ({
        card: {
            boxShadow: 'none !important',
            backgroundColor: '#f8f8f8 !important',
            padding: '30px 20px 20px 20px',
            '&:hover': {
                cursor: 'pointer',
                opacity: 0.5
            },
            [theme.breakpoints.down("md")]: {
                display: 'block',
                width: '100%',
                maxWidth: '100% !important',
                marginTop: '24px'
            },
            [theme.breakpoints.up("lg")]: {
                maxWidth: 280
            },
            [theme.breakpoints.down("lg")]: {
                maxWidth: 200
            },
        },
        avatar: {
            backgroundColor: '#ed5f5e !important',
            borderRadius: '2% !important'
        }
    }))

    const classes = useStyles(props);

    return (
            <Card className={classes.card}>
                <CardHeader
                    avatar={
                    <Avatar className={classes.avatar} aria-label="recipe">
                        <props.icon/>
                    </Avatar>
                    }
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {props.title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        On at tolerably depending do perceived. Luckily eat joy see own shyness minuter.
                        So before remark at depart Did son unreserved.
                    </Typography>
                </CardContent>
            </Card>
    )
}

export default CardItem
