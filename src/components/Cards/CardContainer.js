import React from 'react'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import Box from '@mui/material/Box';
import DesktopWindowsIcon from '@mui/icons-material/DesktopWindows';
import CameraAltIcon from '@mui/icons-material/CameraAlt';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import { makeStyles } from "@material-ui/core/styles";
import ShoppingBagIcon from '@mui/icons-material/ShoppingBag';
import { styled } from "@mui/material/styles";
import CardItem from './CardItem';


function CardContainer(props) {
    const useStyles = makeStyles(theme => ({
        box: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            margin: 40,
            [theme.breakpoints.down("md")]: {
                flexDirection: 'column',
            },
           
        }
    }))

    const classes = useStyles(props);

    return (
        <Box className={classes.box}>
            <CardItem title='WEB DESIGN' icon={DesktopWindowsIcon} />
            <CardItem title='VIDEO' icon={CameraAltIcon} />
            <CardItem title='ADVERTISING' icon={AccountBalanceIcon} />
            <CardItem title='BRANDING' icon={ShoppingBagIcon} />
        </Box>
    )
}

export default CardContainer
