import React from "react";
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import Slider1 from "../../assets/images/sl1.jpg";
import Slider2 from "../../assets/images/sl2.jpg";
import { makeStyles } from "@material-ui/core/styles";
import { Image } from "./Carousel.styled";

function CarouselContainer(props) {
  const useStyles = makeStyles((theme) => ({
    container: {
      width: '100%',
      height: '85%'
    },
  }));

  const classes = useStyles(props);

  return (
    <div>
      <Carousel className={classes.container} nextLabel='' prevLabel=''>
        <Carousel.Item interval={1500}>
          <Image src={Slider1} alt=" One" />
          <Carousel.Caption>
            <h3>Label for first slide</h3>
            <p>Sample Text for Image One</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={1500}>
          <Image src={Slider2} alt=" two" />
          <Carousel.Caption>
            <h3>Label for Second slide</h3>
            <p>Sample Text for Image One</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}

export default CarouselContainer;
