import * as React from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { makeStyles } from '@mui/styles';
import { width } from '@mui/system';

export default function Load(props) {

  const useStyles = makeStyles((theme) => ({
    box: {
      width: '100%'
    }
  }));
  const classes = useStyles(props);
  return (
    <Box className={classes.box}>
      <LinearProgress />
    </Box>
  );
}