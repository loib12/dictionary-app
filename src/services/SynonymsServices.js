import http from '../http-common'

const get = (word) => {
    return http.get(`${word}/synonyms`)
}

export default { get }


