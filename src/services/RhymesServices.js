import http from '../http-common'

const get = (word) => {
    return http.get(`${word}/rhymes`)
}

export default { get }


