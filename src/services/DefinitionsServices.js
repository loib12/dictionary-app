import http from '../http-common'

const get = (word) => {
    return http.get(`${word}/definitions`)
}

export default { get }


