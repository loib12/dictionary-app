import './App.css';
import Header from './screens/Headers/Header';
import Home from './screens/Home/Home'
import Lookup from './screens/Lookups/Lookup'
import CarouselContainer from './components/Carousels/CarouselContainer';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {

  return (
    <Router>
      <Header />
      <CarouselContainer />
      <div className="app">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/lookup" component={Lookup} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
