import React from 'react';
import Navbar from '../../components/Navigations/Navbar'

function Header(props) {
    return (
        <header>
            <Navbar />
        </header>
    );
}

export default Header;