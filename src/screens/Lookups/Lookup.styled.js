import styled from "styled-components";


export const Form = styled.div `
    display: flex;
    justify-content: space-between;
    max-width: 350px;
    margin: 24px auto 24px auto;
    background-color: #f8f8f8 !important;
`