import TextField from "@material-ui/core/TextField";
import React, { useState } from "react";
import { Button, Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { listSynonymsAction } from "../../actions/SynonymsAction";
import { listDefinitionsAction } from "../../actions/DefinitionsAction";
import { useDispatch, useSelector } from "react-redux";
import SynonymsResult from "../../components/ListResults/SynonymsResult";
import DefinitionsResult from "../../components/ListResults/DefinitionsResult";
import Load from "../../components/Loads/Load";
import Error from "../../components/Errors/Error";
import { listRhymesSelector } from '../../reducers/RhymesReducer'
import { RhymesActions } from '../../actions/RhymesAction'
import RhymesResult from "../../components/ListResults/RhymesResult";
import ErrorRhymes from "../../components/Errors/ErrorRhymes";
import { Form } from "./Lookup.styled";

function Lookup(props) {
  const dispatch = useDispatch();
  const [word, setWord] = useState("");

  const listSynonyms = useSelector((state) => state.listSynonyms);
  const {
    loading: loadingSynonyms,
    data: dataSynonyms,
    error: errorSynonyms,
  } = listSynonyms;

  const listDefinitions = useSelector((state) => state.listDefinitions);
  const {
    loading: loadingDefinitions,
    data: dataDefinitions,
    error: errorDefinitions,
  } = listDefinitions;

  const listRhymes = useSelector((listRhymesSelector))
  const { loading: loadingRhymes, data: dataRhymes, error: errorRhymes } = listRhymes;

  const onTextChange = (e) => setWord(e.target.value);
  const handleSubmit = () => {
    if (word !== "") {
      dispatch(listSynonymsAction(word));
      dispatch(listDefinitionsAction(word));
      dispatch({ type: RhymesActions.RHYMES_LIST_REQUEST, word})
      setWord("");
    } else {
      setWord("");
      return;
    }
  };

  const useStyles = makeStyles((theme) => ({
    box: {
      display: "flex",
      flexDirection: "column",
    },
    list: {
      display: "flex",
      maxWidth: "350px",
      margin: "24px auto 24px auto",
      backgroundColor: "#f8f8f8 !important",
      border: "0.1em solid #000000",
      borderRadius: "0.12em",
    },
    button: {
      display: "inline-block",
      padding: "0.46em 1.6em",
      border: "0.1em solid #000000",
      borderRadius: "0.12em",
      margin: "0 0.2em 0.2em 0",
      boxSizing: "border-box",
      textDecoration: "none",
      fontFamily: "Roboto",
      fontWeight: "300",
      color: "#000000",
      textShadow: "0 0.04em 0.04em rgba(0,0,0,0.35)",
      backgroundColor: "#FFFFFF",
      textAlign: "center",
      transition: "all 0.15s",
      "&:hover": {
        textShadow: "0 0 2em rgba(255,255,255,1)",
        borderColor: "#FFFFFF",
      },
    },
    input: {
      width: "75%",
      padding: "0.46em 1.6em",
      margin: "0 0.2em 0.2em 0",
      boxSizing: "border-box",
      borderRadius: "0.12em",
      border: "0.1em solid #000000",
    },
    pl: {
      paddingLeft: '20px !important'
    }
  }));

  const classes = useStyles(props);
  return (
    <Box>
      <Form>
        <TextField
          className={classes.input}
          onChange={onTextChange}
          value={word}
          placeholder={"type some words"} 
        />
        <Button onClick={handleSubmit} className={classes.button}>
          Lookup
        </Button>
      </Form>

      <div className={classes.list}>
        {loadingDefinitions && loadingSynonyms ? (
          <Load />
        ) : errorDefinitions && errorSynonyms ? (
          <Error errorDefinitions={errorDefinitions} errorSynonyms={errorSynonyms} />
        ) : (
          <ul className={classes.pl}>
            <SynonymsResult category="Synonyms of word" data={dataSynonyms} />
            <DefinitionsResult
              category="Definitions of word"
              data={dataDefinitions}
            />
          </ul>
        )}
      </div>

      <div className={classes.list}>
        {
          loadingRhymes ? <Load />
          : !dataRhymes.hasOwnProperty('rhymes') ? <ErrorRhymes severity='info' children='please type your word' /> 
          : !dataRhymes.rhymes.hasOwnProperty("all") ? <ErrorRhymes severity='error' children='cant found your word' />
          : errorRhymes ? <ErrorRhymes severity='error' children='Error 404' />
          : 
            <ul>
              <RhymesResult category="Rhymes of word" data={dataRhymes}/> 
            </ul>
        }
      </div>
    </Box>
  );
}

export default Lookup;
