import React from 'react'
import CardItem from '../../components/Cards/CardItem'
import CardContainer from '../../components/Cards/CardContainer'

function Home() {
    return (
        <>
            <CardContainer />
        </>
    )
}

export default Home
