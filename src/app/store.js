import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from 'redux-saga';
import { SynonymsListReducer } from "../reducers/SynonymsReducer";
import { DefinitionsListReducer } from "../reducers/DefinitionsReducer"
import RhymesListReducer from '../reducers/RhymesReducer'
import rootSaga from "./rootSaga";

const sagaMiddleware = createSagaMiddleware();

export default configureStore({
    reducer: {
      listSynonyms: SynonymsListReducer,
      listDefinitions: DefinitionsListReducer,
      listRhymes: RhymesListReducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware)
  });


sagaMiddleware.run(rootSaga)
