import { all } from 'redux-saga/effects'
import RhymesSaga from '../sagas/RhymesSaga'

function* helloSaga() {
    //console.log('Hello saga')
}

export default function* rootSaga() {
    console.log('root saga is running')
    yield all([
        helloSaga(),
        RhymesSaga()
    ])
}