import { createSlice } from '@reduxjs/toolkit'

const initialState  = {
    loading: false,
    data: {},
}

const RhymesSlice = createSlice({
    name: 'rhymes',
    initialState,
    reducers: {
        listRhymesRequest: (state, action) => {
            state.loading = true;
        },
        listRhymesSuccess: (state, action) => {
            state.loading = false;
            state.data = action.payload;
        },
        listRhymesFail: (state, action) => {
            state.loading = false;
            state.error = action.payload;
        }
    }
});

export const {
    listRhymesRequest,
    listRhymesSuccess,
    listRhymesFail
} = RhymesSlice.actions

export const listRhymesSelector = state => state.listRhymes

export default RhymesSlice.reducer


