import { SYNONYMS_LIST_REQUEST, SYNONYMS_LIST_SUCCESS, SYNONYMS_LIST_FAIL} from '../constants/synonymsConstant'
const initialState = {
    loading: false,
    data: {},
    error: {}
}
export const SynonymsListReducer = (state = initialState, action) => {
    switch (action.type) {
        case SYNONYMS_LIST_REQUEST:
            return { loading: true }
        case SYNONYMS_LIST_SUCCESS:
            return { loading: false, data: action.payload }
        case SYNONYMS_LIST_FAIL:
            return { loading: false, error: action.payload}
        default:
            return state
    }
}