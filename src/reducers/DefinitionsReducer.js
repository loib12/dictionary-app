import { DEFINITIONS_LIST_REQUEST, DEFINITIONS_LIST_SUCCESS, DEFINITIONS_LIST_FAIL } from '../constants/definitionsConstant'

const initialState = {
    loading: false,
    data: {},
    error: {}
}

export const DefinitionsListReducer = (state = initialState, action) => {
    switch (action.type) {
        case DEFINITIONS_LIST_REQUEST:
            return { loading: true }
        case DEFINITIONS_LIST_SUCCESS:
            return { loading: false, data: action.payload }
        case DEFINITIONS_LIST_FAIL:
            return { loading: false, error: action.payload}
        default:
            return state
    }
}