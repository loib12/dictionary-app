import { createTheme } from '@mui/material/styles';

const theme = createTheme({
    color: {
        primary: '#6c6f00',
        secondary: '#d2ce56',
        error: '#bc5100'
    },
    typography: {
        fontFamily: 'Montserrat'
    },
    shape: {
        borderRadius: 4,
        backgroundColor: 'red',
        textColor: '#fff',
        border: '#cccccc'
    }
})

export default theme